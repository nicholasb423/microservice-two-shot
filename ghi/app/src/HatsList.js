import React, {useEffect, useState } from 'react';

function HatsList() {
    const [hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          console.log(data)
          setHats(data.hats)
        }
    }
    useEffect(() => {
        fetchData()
      }, []);
    return(
        <table className='table table-stripped'>
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat =>{
                    return (
                        <tr>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default HatsList;
