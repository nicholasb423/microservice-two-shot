import React, {useEffect, useState} from 'react';


function ShoeList() {

    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          console.log(data);
          setShoes(data.shoes);
        }
      }

      useEffect(() => {
        fetchData()
      }, []);
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacture</th>
            <th>Model</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr>
                <td>{ shoe.manufacture }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default ShoeList;
