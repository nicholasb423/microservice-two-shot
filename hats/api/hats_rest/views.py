from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hats, LocationVO

class HatsModelEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url"
    ]
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

# Create your views here.

require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsModelEncoder,
        )
    else:
        content = json.loads(request.body)
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsModelEncoder,
            safe=False,
        )
