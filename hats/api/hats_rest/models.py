from django.db import models

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

class Hats(models.Model):
    fabric = models.CharField(max_length=150)
    style_name = models.CharField(max_length=150)
    color= models.CharField(max_length=150)
    picture_url = models.URLField(null=True)
    # location = models.ForeignKey(
    #     LocationVO,
    #     related_name="location",
    #     on_delete=models.CASCADE,
    # )
