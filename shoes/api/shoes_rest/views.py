from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoes
from common.json import ModelEncoder
from django.http import JsonResponse
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
    ]

class ShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacture",
        "model_name",
        "color",
        "picture",
    ]

@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesEncoder,
        )
    else:
        content = json.loads(request.body)
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoesEncoder,
            safe=False,
        )
    else:
        request.method == "DELETE"
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
